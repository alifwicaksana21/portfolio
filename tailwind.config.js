/** @type {import('tailwindcss').Config} */
export default {
  content: [],
  plugins: [require("@tailwindcss/typography"), require("daisyui")],
  daisyui: {
    themes: [
      {
        default: {
          primary: "#FB6107",
          secondary: "#F19953",
          accent: "#FFEC51",
          neutral: "#2F2F2F",
          "base-100": "#E6FAFC",
        },
      },
    ],
  },
};
